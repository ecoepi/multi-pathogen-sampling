### Loading Libraries ###
library(dplyr)
library(tidyverse)
library(rlang)
library(ggplot2)
library(plotly)

### Setting Working directory ###
# setwd("C:/Users/cortijo/OneDrive - EFSA/H/Projects/Plant Health/Sybren/PPSurv/Crop Optimization/")
 setwd("C:/Users/hanst/Nextcloud/Cloud/ActiveWork/efsa/SurveyProt/")
# setwd("C:\\Users\\mlange\\Desktop\\temp\\multipest")

### Reading table containing the PROBLEM to be optimized ###
# opt <- read.csv("CropTable.csv", header = TRUE, fileEncoding = "UTF-8")
opt <- read.csv("samplingMatrix.csv", header = TRUE, fileEncoding = "UTF-8")

# Input manthly capacity limites
# limits <- c(300, 400, 450, 500, 750, 400, 100, 0, 400, 400, 600, 250)
# limits <- c(0, 0, 0, 350, 592, 0, 0, 0, 440, 400, 660, 250)
## Test Jose
limits <- c(150, 250, 400, 700, 600, 200, 50, 0, 150, 200, 150, 50)

# this version converts input matrices into integer IDs which then are structured regarding inclusiveness. 
# The amended optimasation then starts satisfying requests of least inclusive matrices (e.g. fruits & shoots) 
# before following the line of increasing inclusiveness (-> fruit, shoot -> fruit || shoot)

# @Dagmar NEEDS INTERFACING 
# mapping from matrix strings to IDs
matrix_map <- c(
  "fruits | shoots" = 1,
  "fruits or shoots" = 1,
  "shoots | fruits" = 1,
  "shoots or fruits" = 1,

  "fruits" = 2,

  "shoots" = 3,

  "fruits & shoots" = 4,
  "fruits and shoots" = 4,
  "shoots & fruits" = 4,
  "shoots and fruits" = 4
)

# this is application dependent and expresses inclusiveness. 
# specify the rank order of the ID. In this case 1 incl. 2 or 3 includes 4
# This is arbitrary between the ID given above and the rank order here
matrix_rank <- c(4,2,2,1)

# @Dagmar
# the reverse translation from ID to matrix names for output
# mapping from matrix IDs to strings
id_map <- c(
  "fruits | shoots",
  "fruits",
  "shoots",
  "fruits & shoots"
)

# @Dagmar
# Which matrix can re-use what
# First index (row) is "who uses", second index (column) is "what would be used"
can_reuse <- matrix(
  c(TRUE, TRUE, TRUE, TRUE, # fruits | shoots
  FALSE, TRUE, FALSE, TRUE, # fruits
  FALSE, FALSE, TRUE, TRUE, # shoots
  FALSE, FALSE, FALSE, TRUE), # fruits & shoots
  nrow = 4, ncol = 4, byrow=TRUE)

### Converting the months into R months ordered ###
opt$Time <- factor(opt$Time,levels=month.name)
### Converting the months into numeric ###
opt$Time <- as.numeric(opt$Time)

### Sorting the data based on Pest, Matrix and Time ###
#opt <- opt %>% arrange(Pest,Matrix,Time)

opt <- opt %>%
  group_by(Pest) %>%
  mutate(months = list(Time))

### Selecting only the columns needed ###
opt3 <- opt %>% select("Pest","Matrix","Sample.size","Time","months") %>%
  rename(pest    = "Pest",
  matrix_name = "Matrix",
  samples = "Sample.size")

# The graphical illustration of the problem
p <- ggplot(opt3,aes(ymin=Time,ymax=Time+1,x=pest,text = paste("Period:", paste(opt3$months,sep = " "), "<br>",
  # paste(month.name[opt3$from], "-",month.name[opt3$until]), 
  "Sample Size:", opt3$samples))) + 
  geom_linerange(size=log10(opt3$samples-(min(opt3$samples)-10))) + 
  scale_y_continuous(breaks=seq(4,12)) + 
  coord_flip() + facet_wrap(~matrix_name) + xlab("Pest") + ylab("Months")

ggplotly(, tooltip = c("x", "y", "text"))

opt2 <- opt3[!duplicated(opt3[,names(opt3)!="Time"]),names(opt3)!="Time"]


# @Dagmar
# translate matrix names to IDs as defined in l.29ff
opt2["matrix_"] <- 0
for (i in 1:nrow(opt2)){
  opt2$matrix_[i] <- matrix_map[tolower(opt2$matrix_name[i])]
}
opt2 <- opt2 %>% select("pest","matrix_", "samples", "months")

### Reading in the PROBLEM ###
problem <- list(NULL)
for (i in 1:nrow(opt2)){
    problem[[i]] <- as.list(opt2[i,])
}

# target function. 
# this version adds all trips and samples in a proposed SOLUTION
target <- function(solution) {
  trips <- rep(0, 52)
  samples <- 0
  # going through all elements of SOLUTION by counter ACTION
  for (action in solution) {
    trips[action$at] <- 1
    samples <- samples + action$samples
  }
  c(sum(trips), samples)
}

# Comparison of 2 SOLUTIONS by the required "#trips" (lhs) and "#samples" (rhs)
target_less_than <- function(lhs, rhs) {
  (lhs[1] < rhs[1]) || (lhs[1] == rhs[1] && lhs[2] < rhs[2])
}

target_less_equal <- function(lhs, rhs) {
  (lhs[1] <= rhs[1]) || (lhs[1] == rhs[1] && lhs[2] <= rhs[2])
}

# @Dagmar - from here its like copy-paste?

# @Dagmar
# define reusable samples according to the inclusiveness matrix above
# Requirement is element of PROBLEM, ACTION is counting through the elelemnts of tested SOLUTION
matches <- function(requirement, action) {
  return( can_reuse[requirement$matrix_, action$matrix_] )
}

# prepare entities to manage temporary and final solutions
best_solution <- list()
preserved <- list()
preserved_temp <- list()
any_solution <- FALSE

# construct trivial solution
# using maximum trips (here 12 months, but can be #time entries)
# using sum of all samples in the PROBLEM 
total_samples <- 0
for (requirement in problem) {
  total_samples <- total_samples + requirement$samples
}
best_target_value <- c(12, total_samples)

# formating tools for the output
solution_to_table <- function(solution) {
  best_sol<-data.frame(matrix(unlist(solution),nrow=length(solution),ncol=6,byrow=T))
  names(best_sol)<-c("Pest","MatrixID","Samples","Time","Reference", "From")
  
  best_sol$MatrixID <- as.integer(best_sol$MatrixID)
  best_sol$Samples <- as.integer(best_sol$Samples)
  best_sol$Time <- as.integer(best_sol$Time)
  
  best_sol<-best_sol[order(best_sol$Time, best_sol$MatrixID, best_sol$Pest, best_sol$From),]
  
  best_sol["Matrix"] <- ""
  for (i in 1:nrow(best_sol)){
    best_sol$Matrix[i] <- id_map[best_sol$MatrixID[[i]]]
  }
  best_sol <- best_sol %>% select("Pest","Matrix","Samples","Time","Reference", "From")

  best_sol
}

# MAIN FUNCTION of RECCURSIVE ALGORITHM
# recursively construct an optimal solution i.e. according to the target function
satisfy <- function(solution) {
  # is the proposed SOLUTION better than the one stored in best_sol
  # fewer trips will always be preferred, if equal trips fewer samples are preferred 
  target_value <- target(solution)
  if (!target_less_than(target_value, best_target_value)) {
    return()
  }
  
  # prepared choosing the next REQUIREMENT to treat of the PROBLEM
  unsatisfied_requirement <- NULL
  required_samples <- NULL
  
  month_samples <- limits
  
  for (requirement in problem) {
    samples <- requirement$samples
    for (action in solution) {
      # only if the element of solution is for one of the time windows of the required
      if (! (action$at %in% requirement$months[[1]])) {
        next()
      }
      # only if the matrices of element of solution can be reused by the required sampling
      if (!matches(requirement, action)) {
        next()
      }
      
      # are both for same pest?
      own_sample <- action$pest == requirement$pest
      equivalent_samples <- action$samples
      
      # smaller sample number of the entry in solution (action) 
      # and that (remaining of) required  
      equivalent_samples <- min(equivalent_samples, samples)

      # update the samples still required
      if (own_sample) {
        equivalent_samples <- min(equivalent_samples, month_samples[action$at])
      }
      samples <- samples - equivalent_samples
      
      # technical for controlling output
      # record source pest or mark as first use by "-"
      if (equivalent_samples > 0) {
        preserved_temp[[length(preserved_temp) + 1]] <<- list(
          pest = requirement$pest,
          matrix_ = requirement$matrix_,
          samples = equivalent_samples,
          at = action$at,
          reference = requirement$samples,
          from = if(own_sample) "-" else action$pest
        )
        if (own_sample) {
          month_samples[action$at] <- month_samples[action$at] - equivalent_samples
        }
      }
      if (samples == 0) {
        break()
      }
    }
    
    # still samples need for this REQUIREMNT
    if (samples != 0) {
    #chose most inclusive matrix according to rank value, then search the largest sample request pending
      if (is.null(unsatisfied_requirement)) {
          unsatisfied_requirement <- requirement
          required_samples <- samples
      } else {
        rank_new <- matrix_rank[requirement$matrix_]
        rank_old <- matrix_rank[unsatisfied_requirement$matrix_]
        # Chose the most reusable matrix first, and prefer larger samples within a matrix
        if (rank_new < rank_old || (rank_new == rank_old && samples > required_samples)) {
          unsatisfied_requirement <- requirement
          required_samples <- samples
        }
      }
    }
  }
  # store satisfied to solution if capacity allows
  if (!is.null(unsatisfied_requirement)) {
    requirement <- unsatisfied_requirement
    samples <- required_samples
    
    for (at in requirement$months[[1]]) {
        if (month_samples[at] <= 0) {
          next()
        }
        max_samples <- min(samples, month_samples[at])

        solution[[length(solution) + 1]] <- list(
          pest = requirement$pest,
          matrix_ = requirement$matrix_,
          samples = max_samples,
          at = at,
          reference = requirement$samples,
          from = "-"
        )
        
        preserved_temp <<- list()
        satisfy(solution)
        
        length(solution) <- length(solution) - 1
    }
    if (length(solution) == 0) {
      #print("no solution")
    }
    # if all requirements addressed in the extended solution
    # and the target function improves then record the improvement
  } else {
    if (target_less_than(target_value, best_target_value)) {
      best_target_value <<- target_value
      best_solution <<- solution
      preserved <<- preserved_temp
      preserved_temp <<- list()
      any_solution <<- TRUE
    }
  }
}

### Start
satisfy(list())
### all processed

# final summary outputs
if (!any_solution) {
  stop("no solution")
}
best <- solution_to_table(preserved)
best_sol <- solution_to_table(best_solution)

required <- best[best$From == "-",]

print(aggregate(Samples ~ Time + Matrix, data=required, FUN=sum)) # ToDo Table Matrices by Time Window
print(aggregate(Samples ~ Time, data=required, FUN=sum))          # Effort Table by Time Window
print(limits)                                                     # included Capacities
print(required)                                                   # where does the ToDo come from that satisfies all
#best <- best %>% arrange(Pest,Time) ###,Matrix,Time)
best_sol <- best_sol %>% arrange(Pest,Time) ###,Matrix,Time)

print("Solution")
print(best_sol)                                                   # as before  
print("Preserved")
print(best)                                                       # all details of the solution incl. source and reuse
